-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Creato il: Apr 20, 2018 alle 08:10
-- Versione del server: 10.1.21-MariaDB
-- Versione PHP: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `automobili`
--

-- --------------------------------------------------------

--
-- Struttura della tabella `automobili`
--

CREATE TABLE `automobili` (
  `ID_Automobile` int(10) NOT NULL,
  `NomeModello` varchar(255) DEFAULT NULL,
  `AnnoImmatricolazione` int(10) DEFAULT '0',
  `Targa` varchar(7) DEFAULT NULL,
  `Telaio` varchar(15) DEFAULT NULL,
  `Nome` varchar(255) DEFAULT NULL,
  `Cognome` varchar(255) DEFAULT NULL,
  `Intervento` varchar(255) DEFAULT NULL,
  `DataIngresso` date DEFAULT NULL,
  `DataUscita` date DEFAULT NULL,
  `ID_Marca` int(10) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `automobili`
--

INSERT INTO `automobili` (`ID_Automobile`, `NomeModello`, `AnnoImmatricolazione`, `Targa`, `Telaio`, `Nome`, `Cognome`, `Intervento`, `DataIngresso`, `DataUscita`, `ID_Marca`) VALUES
(5, '<xzc<xc', 2016, 'fdgsfg', 'dfgsfg', 'sfdgsfgs', 'fgsdfgsdfg', 'sdfgsfg', '2012-09-09', '2017-04-15', 2),
(7, 'quello', 2013, 'adfadf', 'ads', 'asdaf', 'fsfda', 'sdfasd', '2017-10-02', '1970-01-01', 1);

-- --------------------------------------------------------

--
-- Struttura della tabella `marche`
--

CREATE TABLE `marche` (
  `ID_Marca` int(10) NOT NULL,
  `NomeMarca` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `marche`
--

INSERT INTO `marche` (`ID_Marca`, `NomeMarca`) VALUES
(1, 'Audi'),
(2, 'Fiat'),
(3, 'Volvo'),
(4, 'BMW'),
(5, 'MAserati');

-- --------------------------------------------------------

--
-- Struttura della tabella `utenti`
--

CREATE TABLE `utenti` (
  `ID_Utente` int(11) NOT NULL,
  `NomeUtente` varchar(18) DEFAULT NULL,
  `Password` varchar(18) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `utenti`
--

INSERT INTO `utenti` (`ID_Utente`, `NomeUtente`, `Password`) VALUES
(1, 'tizio', 'caio'),
(3, 'Simo', 'Simo'),
(4, 'Pippo', 'Pippo');

--
-- Indici per le tabelle scaricate
--

--
-- Indici per le tabelle `automobili`
--
ALTER TABLE `automobili`
  ADD PRIMARY KEY (`ID_Automobile`),
  ADD KEY `ID_Marca` (`ID_Marca`);

--
-- Indici per le tabelle `marche`
--
ALTER TABLE `marche`
  ADD PRIMARY KEY (`ID_Marca`);

--
-- Indici per le tabelle `utenti`
--
ALTER TABLE `utenti`
  ADD PRIMARY KEY (`ID_Utente`);

--
-- AUTO_INCREMENT per le tabelle scaricate
--

--
-- AUTO_INCREMENT per la tabella `automobili`
--
ALTER TABLE `automobili`
  MODIFY `ID_Automobile` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT per la tabella `marche`
--
ALTER TABLE `marche`
  MODIFY `ID_Marca` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT per la tabella `utenti`
--
ALTER TABLE `utenti`
  MODIFY `ID_Utente` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- Limiti per le tabelle scaricate
--

--
-- Limiti per la tabella `automobili`
--
ALTER TABLE `automobili`
  ADD CONSTRAINT `Automobili_fk1` FOREIGN KEY (`ID_Marca`) REFERENCES `marche` (`ID_Marca`) ON DELETE SET NULL ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
